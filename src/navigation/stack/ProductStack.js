import React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import ListScreen from '../../screens/products/ListScreen';
import ProductScreen from '../../screens/products/ProductScreen';
import PurchaseScreen from '../../screens/products/PurchaseScreen';
import EditProductScreen from '../../screens/products/EditProductScreen';

const Stack = createStackNavigator();

const ProductStack = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen name="List" component={ListScreen}/>
            <Stack.Screen name="Product" component={ProductScreen}/>
            <Stack.Screen name="Purchase" component={PurchaseScreen} />
            <Stack.Screen name="EditProduct" component={EditProductScreen}/>
        </Stack.Navigator>
    )
}

export default ProductStack;