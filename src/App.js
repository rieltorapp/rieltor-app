import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import AuthStack from './navigation/stack/AuthStack';
import MainTab from './navigation/tab/MainTab';

export default function App() {
  const [isUserToken, setUserToken] = useState(true);
  return (
    <NavigationContainer>
      {isUserToken ? 
        <MainTab/>
      :
        <AuthStack />
      }
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
