import React from 'react';
import {Text, View, StyleSheet, Button} from 'react-native';

const AddProductScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>Add product Screen</Text>
        </View>
    )
}

export default AddProductScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})