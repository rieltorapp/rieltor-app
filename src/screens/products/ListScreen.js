import React from 'react';
import {Text, View, StyleSheet, Button, TouchableHighlight} from 'react-native';

const ListScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>List Screen</Text>
            <Button 
            title="Detail"
            onPress={()=>props.navigation.navigate('Product')}
            />
        </View>
    )
}

export default ListScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})