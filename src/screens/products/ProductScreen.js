import React from 'react';
import {Text, View, StyleSheet, Button} from 'react-native';

const ProductScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>Product Screen</Text>
            <Button 
            title="Buy"
            onPress={()=>props.navigation.navigate('Purchase')}
            />
            <Button 
            title="Edit product"
            onPress={()=>props.navigation.navigate('EditProduct')}
            />
            <Button 
            title="Go back"
            onPress={()=>props.navigation.goBack()}
            />
        </View>
    )
}

export default ProductScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})