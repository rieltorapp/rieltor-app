import React from 'react';
import {Text, View, StyleSheet, Button, TouchableHighlight} from 'react-native';

const ProfileScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>Profile Screen</Text>
            <Button
            title="Edit"
            onPress={()=>props.navigation.navigate('ProfileEdit')}
            />
        </View>
    )
}

export default ProfileScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})