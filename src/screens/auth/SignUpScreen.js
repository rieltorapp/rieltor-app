import React from 'react';
import {Text, View, StyleSheet, Button, TouchableHighlight} from 'react-native';

const SignUpScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>Sign up Screen</Text>
            <Button 
            title="Back"
            onPress={()=>props.navigation.goBack()}
            />
        </View>
    )
}

export default SignUpScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})