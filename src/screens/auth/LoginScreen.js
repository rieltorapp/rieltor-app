import React from 'react';
import {Text, View, StyleSheet, Button, TouchableHighlight} from 'react-native';

const LoginScreen = (props) => {
    return (
        <View style={styles.screenContent}>
            <Text>Login Screen</Text>
            <Button 
            title="Detail"
            onPress={()=>props.navigation.navigate('SignUp')}
            />
        </View>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    screenContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})